package com.example.katana.cafemanagementsystem_android;

/**
 * Created by KaTaNa on 8/25/2016.
 */
public class Food {
    private int _id = 0;
    private String _name = "noName";
    private String _price = "noPrice";
    private String _imagePath = "noImagePath";
    private String _afterTaste = "noAfterTaste";
    private int _rating = 0; // Rating is a number between 0 and 5
    private String[] _comments;
    private int _preparationTime = 0; // This value is in Minutes
    private String _description = "noDescription";

    public Food() {
        this._comments = new String[3];
    }

    public void setId(int inId) {
        this._id = inId;
    }

    public void setName(String inName) {
        this._name = inName;
    }

    public void setPrice(String inPrice) {
        this._price = inPrice;
    }

    public void setImagePath(String inImagePath) {
        this._imagePath = inImagePath;
    }

    public void setAfterTaste(String inAfterTaste) {
        this._afterTaste = inAfterTaste;
    }

    public void setRating(int inRating) {
        if (inRating <= 5 && inRating >= 0)
            this._rating = inRating;
        else
            throw new IllegalArgumentException();
    }

    public void setPreparationTime(int inPreparationTime) {
        if (inPreparationTime <= 60 && inPreparationTime >= 1)
            this._preparationTime = inPreparationTime;
        else
            throw new IllegalArgumentException();
    }

    public void setDescription(String inDescription) {
        this._description = inDescription;
    }

    public int getId() {
        return this._id;
    }

    public String getName() {
        return this._name;
    }

    public String getPrice() {
        return this._price;
    }

    public String getImagePath() {
        return this._imagePath;
    }

    public String getAfterTaste() {
        return this._afterTaste;
    }

    public int getRating() {
        return this._rating;
    }

    public int getPreparationTime() {
        return this._preparationTime;
    }

    public String getDescription() {
        return this._description;
    }

    // This method concatenates comments related to one food to be sent as an attribute of sending JSON
    public String commentCombiner() {
        String combinedComments = "";
        for (int i = 0; i < this._comments.length; i++) {
            combinedComments = combinedComments + "#" + this._comments[i];
        }
        return combinedComments;
    }

    // This method separates receiving comments related to one food to be saved in Food Class
    public void commentSeparator(String inCombinedComments) {
        char[] charArray = inCombinedComments.toCharArray();
        int i, j = 0, k = 0, sharp;
        for (i = 0; i < 3; i++) {
            for (; ; j++) {
                if (charArray[j] == '#') {
                    sharp = j;
                    j++;
                    break;
                }
            }
            for (; k < sharp; k++) {
                this._comments[i] = this._comments[i] + charArray[k];
                k = sharp + 1;
            }
        }
    }
}
