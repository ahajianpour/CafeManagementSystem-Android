package com.example.katana.cafemanagementsystem_android;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.gson.Gson;

import java.io.Closeable;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // sending request for AllFoods and getting it
        HTTPReqRes reqres = new HTTPReqRes("192.168.1.25", "1500", "/allfoods");
        Food[] myFoods = new Gson().fromJson(reqres._message, Food[].class);

        // inserting it to the database
        FoodDatabaseHelper myHelper = new FoodDatabaseHelper(this);
        for (Food food :
                myFoods) {
            myHelper.addFood(food);
        }

        // TODO: calling ViewAdapter to show Foods in UI
    }

    // TODO: a ViewAdapter should be implemented here

    // a nested class to send and receive HTTP messages
    public class HTTPReqRes extends AsyncTask<Void, Void, Void> {
        private String _ipAddress = "";
        private String _port = "";
        private String _command = "";
        public String _message = "";

        // port and IP Address can be implemented static
        public HTTPReqRes(String inIPAddress, String inPort, String inCommand) {
            _ipAddress = inIPAddress;
            _port = inPort;
            _command = inCommand;
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpURLConnection myHttpURLConnection = null;
            try {
                int readByte;
                byte[] buffer = new byte[4096];
                URL url = new URL("http://" + _ipAddress + ":" + _port + _command);
                myHttpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = myHttpURLConnection.getInputStream();
                while (true) {
                    readByte = inputStream.read(buffer);
                    if (readByte == -1) {
                        break;
                    }
                }

                _message = new String(buffer).trim();
                myHttpURLConnection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
                if (myHttpURLConnection != null)
                    myHttpURLConnection.disconnect();
            }
            return null;
        }
    }


    // TODO: this class needs a ViewAdapter
    public void order(int inTableNumber, ArrayList<Food> inFoodList) {

        // getting currently available OrderID from server
        HTTPReqRes reqres = new HTTPReqRes("192.168.1.25", "1500", "/getOrderId");

        // creating the order
        Order myOrder = new Order(Integer.parseInt(reqres._message), inTableNumber, inFoodList);

        //  sending order to server
        reqres = new HTTPReqRes("192.168.1.25", "1500", "/sendOrder" + new Gson().toJson(myOrder));
    }

}
