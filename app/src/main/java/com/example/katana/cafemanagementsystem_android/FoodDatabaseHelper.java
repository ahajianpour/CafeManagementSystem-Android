package com.example.katana.cafemanagementsystem_android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KaTaNa on 8/25/2016.
 */
public class FoodDatabaseHelper extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 3;

    // Database Name
    private static final String DATABASE_NAME = "CafeManagementSystemDatabase";

    // Foods table name
    private static final String TABLE_NAME = "Food";

    // Foods Table Columns names
    private static final String Column_ID = "id";
    private static final String Column_NAME = "name";
    private static final String Column_PRICE = "price";
    private static final String Column_IMAGEPATH = "image";
    private static final String Column_AFTERTASTE = "afterTaste";
    private static final String Column_RATING = "rating";
    private static final String Column_COMMENTS = "comments";
    private static final String Column_PREPARATIONTIME = "preparationTime";
    private static final String Column_DESCRIPTION = "description";

    // Food's Constructor
    public FoodDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_FOODS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
                + Column_ID + " INTEGER PRIMARY KEY, "
                + Column_NAME + " TEXT, "
                + Column_PRICE + " TEXT, "
                + Column_IMAGEPATH + " TEXT, "
                + Column_AFTERTASTE + " TEXT, "
                + Column_RATING + " INTEGER, "
                + Column_COMMENTS + " TEXT, "
                + Column_PREPARATIONTIME + " INTEGER"
                + Column_DESCRIPTION + ")";
        db.execSQL(CREATE_FOODS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new food
    void addFood(Food food) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Column_ID, food.getId()); // Food ID
        values.put(Column_NAME, food.getName()); // Food Name
        values.put(Column_PRICE, food.getPrice()); // Food Price
        values.put(Column_IMAGEPATH, food.getImagePath()); // Food Image
        values.put(Column_AFTERTASTE, food.getAfterTaste()); // Food AfterTaste
        values.put(Column_RATING, food.getRating()); // Food Rating
        values.put(Column_COMMENTS, food.commentCombiner()); // Food Comments
        values.put(Column_PREPARATIONTIME, food.getPreparationTime()); // Food PreparationTime
        values.put(Column_DESCRIPTION, food.getDescription()); // Food Description

        // Inserting Row
        db.insert(TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }

    // Getting single food
    Food getFoodByID(int inID) {
        SQLiteDatabase db = this.getReadableDatabase();
        Food myFood = new Food();

        // Creating query
        Cursor myCursor = db.rawQuery("SELECT * FROM Food WHERE id = " + inID, null);
        myCursor.moveToFirst();

        // Write the goal row to Food object
        do {
            myFood.setId(myCursor.getInt(0));
            myFood.setName(myCursor.getString(1));
            myFood.setPrice(myCursor.getString(2));
            myFood.setImagePath(myCursor.getString(3));
            myFood.setAfterTaste(myCursor.getString(4));
            myFood.setRating(myCursor.getInt(5));
            myFood.commentSeparator(myCursor.getString(6));
            myFood.setPreparationTime(myCursor.getInt(7));
            myFood.setDescription(myCursor.getString(8));

        } while (myCursor.moveToNext());

        // Finalizing the process
        myCursor.close();
        db.close();
        return myFood;
    }

    // Getting All Foods
    public Food[] getAllFoods() {
        List<Food> FoodList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor myCursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (myCursor.moveToFirst()) {
            do {
                Food myFood = new Food();
                myFood.setId(myCursor.getInt(0));
                myFood.setName(myCursor.getString(1));
                myFood.setPrice(myCursor.getString(2));
                myFood.setImagePath(myCursor.getString(3));
                myFood.setAfterTaste(myCursor.getString(4));
                myFood.setRating(myCursor.getInt(5));
                myFood.commentSeparator(myCursor.getString(6));
                myFood.setPreparationTime(myCursor.getInt(7));
                myFood.setDescription(myCursor.getString(8));

                // Adding food to list
                FoodList.add(myFood);
            } while (myCursor.moveToNext());
        }
        Food[] FoodArray;
        FoodArray = FoodList.toArray(new Food[FoodList.size()]);
        // Finalizing the process
        myCursor.close();
        db.close();
        // return food list
        return FoodArray;
    }

    // Updating single food
    public int updateFood(Food food) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Column_ID, food.getId()); // Food ID
        values.put(Column_NAME, food.getName()); // Food Name
        values.put(Column_PRICE, food.getPrice()); // Food Price
        values.put(Column_IMAGEPATH, food.getImagePath()); // Food Image
        values.put(Column_AFTERTASTE, food.getAfterTaste()); // Food AfterTaste
        values.put(Column_RATING, food.getRating()); // Food Rating
        values.put(Column_COMMENTS, food.commentCombiner()); // Food Comments
        values.put(Column_PREPARATIONTIME, food.getPreparationTime()); // Food PreparationTime
        values.put(Column_DESCRIPTION, food.getDescription()); // Food Description

        // updating row
        return db.update(TABLE_NAME, values, Column_ID + " = ?",
                new String[]{String.valueOf(food.getId())});
    }

    // Deleting single food
    public void deleteFood(Food food) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, Column_ID + " = ?",
                new String[]{String.valueOf(food.getId())});
        db.close();
    }

    // Getting foods Count
    public int getFoodsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }
}
