package com.example.katana.cafemanagementsystem_android;

import java.util.ArrayList;

/**
 * Created by KaTaNa on 8/27/2016.
 */
public class Order {
    private int _id;
    private int _tableNumber;
    private ArrayList<Food> _foodList;

//    public Order(int inTableNumber, ArrayList<Food> inFoodOrder) {
//        this._tableNumber = inTableNumber;
//        this._foodList = inFoodOrder;
//    }

    public Order(int inId, int inTableNumber, ArrayList<Food> inFoodOrder) {
        this._id = inId;
        this._foodList = inFoodOrder;
        this._tableNumber = inTableNumber;
    }

    public int getOrderId() {
        return this._id;
    }

    public int getTableNumber() {
        return this._tableNumber;
    }

    public void setOrderId(int inOrderId) {
        this._id = inOrderId;
    }

    public void setTableNumber(int inTableNumber) {
        this._tableNumber = inTableNumber;
    }
}
